package cryptofr

import (
	"math/big"
	"hash"
	"APSS/shamir"
	"sync"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
	"github.com/consensys/gnark-crypto/ecc"
)


type CipherMR struct {
	C1 curve.G1Jac
	C2 curve.G1Jac
}



func EncryptMR(hFunc hash.Hash, t *big.Int, n *big.Int,  keys []curve.G1Jac, secret curve.G1Jac, G curve.G1Jac, GAff curve.G1Affine, FFT bool, multi bool) ([]CipherMR, []*fr.Element, []fr.Element, []curve.G1Jac){

	cipher := make([]CipherMR, int(n.Int64()))
	rdArray := make([]*fr.Element, int(n.Int64()))
	polEnc, _ , shares, Eval := shamir.CreatePoly(t, n , secret, GAff, FFT )


	for j := 0; j < int(n.Int64()); j++ {
		// Generete randomness
/*		hFunc.Reset()
		rd := RandStringBytesMaskImprSrc(20)
		hFunc.Write([]byte(rd))
		rd1 := hFunc.Sum(nil)
		rdBig := new(big.Int)
		rdBig.SetBytes(rd1)
*/		var rd fr.Element
		(&rd).SetRandom()

		rdArray[j] = &rd
		var rdBig big.Int
		var C curve.G1Jac
		C.ScalarMultiplication(&keys[j], (&rd).BigInt(&rdBig))
		C.AddAssign(&shares[j])
		cipher[j].C1 = C

		var E curve.G1Jac
		E.ScalarMultiplication(&G, &rdBig)
		cipher[j].C2 = E
	}

	return cipher, rdArray, polEnc, Eval
}


func DecryptMR(n *big.Int, cipher []CipherMR, PrivateKeys []*fr.Element, FFT bool) (curve.G1Jac) {

	resharedDec := make([]curve.G1Jac, len(PrivateKeys))

	for j := 0; j < len(PrivateKeys); j++ {
		var bigk big.Int
		var secret curve.G1Jac
		secret.ScalarMultiplication(&cipher[j].C2, PrivateKeys[j].BigInt(&bigk)).Neg(&secret).AddAssign(&cipher[j].C1)
		resharedDec[j]= secret
	}

	var origin fr.Element
	origin.SetZero()
	recReshared := shamir.InterpolLagrangeG1Jac(n, resharedDec, origin, FFT)

	return recReshared

}

func ReshareMR(hFunc hash.Hash, t *big.Int, n *big.Int, index int, cipher []CipherMR, key *fr.Element, NewReceiverKeys []curve.G1Jac, G curve.G1Jac, GAff curve.G1Affine, FFT bool, multi bool) ([]CipherMR , []*fr.Element, []fr.Element, []curve.G1Jac) {

	var secret curve.G1Jac
	var bigk big.Int
	secret.ScalarMultiplication(&cipher[index].C2, key.BigInt(&bigk)).Neg(&secret).AddAssign(&cipher[index].C1)

	NewCipher , rd, polEnc, Eval:= EncryptMR(hFunc, t, n, NewReceiverKeys, secret, G, GAff, FFT, multi)

	return NewCipher, rd, polEnc, Eval
	}

func ReshareCombMR(n *big.Int, t *big.Int, index []int, resharedDec [][]CipherMR, FFT bool, multi bool) ([]CipherMR) {
		var origin fr.Element
		origin.SetZero()

		coeffs := shamir.LagrangeCoefficients(n, index, origin, FFT)

		resharedCipher := make([]CipherMR, int(n.Int64()))

		config := ecc.MultiExpConfig{}
		//resharedCipherTest := make([]CipherMR, int(n.Int64()))

		if multi == true {
			ch := make(chan bool, 1)
			var wg sync.WaitGroup
			wg.Add(int(n.Int64()))

			//tmp := []curve.G1Affine{}
			//tmp2 := []curve.G1Affine{}
			for j := 0; j < int(n.Int64()); j++ {
				go func(j int, ch chan bool) {
					defer wg.Done()

					/*
					// Test with BatchScalarMultiplicationG1
					values := make([]fr.Element, len(index))
	    		for index, value := range coeffs {
	        values[index-1]=  value
	    		}
					vecDecC1 := []curve.G1Jac{}
	    		for i :=0; i < len(values); i++ {
	        vecDecC1 = append(vecDecC1, resharedDec[i][j].C1)
	    		}
					vecDecC2 := []curve.G1Jac{}
	    		for i :=0; i < len(values); i++ {
	        vecDecC2 = append(vecDecC2, resharedDec[i][j].C2)
	    		}
					vecDecC1Aff := make([]curve.G1Affine, len(vecDecC1))
					curve.BatchJacobianToAffineG1(vecDecC1, vecDecC1Aff)

					vecDecC2Aff := make([]curve.G1Affine, len(vecDecC2))
					curve.BatchJacobianToAffineG1(vecDecC2, vecDecC2Aff)


					tmp = append(tmp, curve.BatchScalarMultiplicationG1(&vecDecC1Aff, values))
					tmp2 = append(tmp2, curve.BatchScalarMultiplicationG1(&vecDecC2Aff, values))
					*/

					for i, c := range coeffs {
						var tmp curve.G1Jac
						var tmp2 curve.G1Jac

						var res big.Int
						c.BigInt(&res)

						tmp.ScalarMultiplication(&resharedDec[i-1][j].C1, &res)
						resharedCipher[j].C1.AddAssign(&tmp)
						tmp2.ScalarMultiplication(&resharedDec[i-1][j].C2, &res)
						resharedCipher[j].C2.AddAssign(&tmp2)

						//curve.BatchScalarMultiplicationG1()
					}

				}(j, ch)
				//resharedCipher[j].C1 = curve.BatchJacobianToAffineG1(tmp)
				//resharedCipher[j].C2 = curve.BatchJacobianToAffineG1(tmp2)
			}
			wg.Wait()
		} else {
			for j := 0; j < int(n.Int64()); j++ {


				var tmpTest curve.G1Jac
				var tmp2Test curve.G1Jac

    		values := make([]fr.Element, len(index))
    		for index, value := range coeffs {
        values[index-1]=  value
    		}

				vecDecC1 := []curve.G1Jac{}
    		for i :=0; i < len(values); i++ {
        vecDecC1 = append(vecDecC1, resharedDec[i][j].C1)
    		}
				vecDecC2 := []curve.G1Jac{}
    		for i :=0; i < len(values); i++ {
        vecDecC2 = append(vecDecC2, resharedDec[i][j].C2)
    		}

				vecDecC1Aff := make([]curve.G1Affine, len(vecDecC1))
				vecDecC1Aff=curve.BatchJacobianToAffineG1(vecDecC1)

				vecDecC2Aff := make([]curve.G1Affine, len(vecDecC2))
				vecDecC2Aff=curve.BatchJacobianToAffineG1(vecDecC2)
			

				_, _ =  tmpTest.MultiExp(vecDecC1Aff, values, config)
				resharedCipher[j].C1.AddAssign(&tmpTest)

				_, _ =  tmp2Test.MultiExp(vecDecC2Aff, values, config)
				resharedCipher[j].C2.AddAssign(&tmp2Test)

				/*
				for i, c := range coeffs {


					var tmp curve.G1Jac
					var tmp2 curve.G1Jac

					var res big.Int
					c.BigInt(&res)

					tmp.ScalarMultiplication(&resharedDec[i-1][j].C1, &res)
					resharedCipher[j].C1.AddAssign(&tmp)
					tmp2.ScalarMultiplication(&resharedDec[i-1][j].C2, &res)
					resharedCipher[j].C2.AddAssign(&tmp2)
				}
				*/
			}

		}

		return resharedCipher
	}
