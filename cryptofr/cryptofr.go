package cryptofr

import (
	"math/big"
	"crypto/sha256"
	"time"
	mrand "math/rand"
	"APSS/shamir"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
)

type Key struct {
	PublicKey curve.G1Jac
	PrivateKey *fr.Element
	seed string
}


type CommitteeSharingInstance struct {
	T  *big.Int
	N  *big.Int
	Index []int
	FFT bool
	G curve.G1Jac
	GAff curve.G1Affine
	SenderKeys []Key
	ReceiverKeys []Key
	NewReceiverKeys []Key
	CommonPublicKey curve.G1Jac
	CommonSecretKey *fr.Element
}

/*
// initialize the value of G and returns an instance containing this G1Jac element.
func NewCryptoInstance() *CryptoInstance {
	var g1Jac, _, g1Aff, _ = bn254.Generators()
	var gen curve.G1Jac
	var genAff curve.G1Affine
	// Arbitrary values
	gN, _ := new(big.Int).SetString("0819273127312094372430948574057405832043109384328572405845907273418304839044650", 10)

	return &CryptoInstance{gen.ScalarMultiplication(&g1Jac, gN),  genAff.ScalarMultiplication(&g1Aff, gN)}
}
*/

func NewCommitteeSharingInstance(t *big.Int, n *big.Int,  index []int, fft bool) (inst *CommitteeSharingInstance) {
	inst = &CommitteeSharingInstance{T: t, N: n, Index : index, FFT: fft}
	inst.initKeys()
	return
}


func (instance *CommitteeSharingInstance) initKeys() {

	var g1Jac, _, g1Aff, _ = curve.Generators()
	var G curve.G1Jac
	var GAff curve.G1Affine
	// Arbitrary values
	gN, _ := new(big.Int).SetString("0819273127312094372430948574057405832043109384328572405845907273418304839044650", 10)

	G.ScalarMultiplication(&g1Jac, gN)
	GAff.ScalarMultiplication(&g1Aff, gN)
	instance.G= G
	instance.GAff= GAff

	instance.SenderKeys = make([]Key, int(instance.N.Int64()))
	for j := 0; j < int(instance.N.Int64()); j++ {
		instance.SenderKeys[j] = KeyGen(G)
	}
	instance.ReceiverKeys = make([]Key, int(instance.N.Int64()))
	for j := 0; j < int(instance.N.Int64()); j++ {
		instance.ReceiverKeys[j] = KeyGen(G)
	}

	instance.NewReceiverKeys = make([]Key, int(instance.N.Int64()))
	for j := 0; j < int(instance.N.Int64()); j++ {
		instance.NewReceiverKeys[j] = KeyGen(G)
	}

	var origin fr.Element
	origin.SetZero()
	coeffs := shamir.LagrangeCoefficients(instance.N, instance.Index, origin, instance.FFT)

	//var commonPublicKey curve.G1Jac
	//var commonSecretKey big.Int
	var zerofr fr.Element
	(&zerofr).SetZero()
	instance.CommonSecretKey=&zerofr

	for i, c := range coeffs {
		var tmp curve.G1Jac
		var res big.Int
		tmp.ScalarMultiplication(&instance.SenderKeys[i-1].PublicKey, c.BigInt(&res))
		instance.CommonPublicKey.AddAssign(&tmp)

		var result fr.Element
		result.Mul(instance.SenderKeys[i-1].PrivateKey, &c)
		instance.CommonSecretKey.Add(instance.CommonSecretKey, &result)
	}
}



var src = mrand.NewSource(time.Now().UnixNano())
var g1Jac, g2Jac, g1Aff, g2Aff = curve.Generators()

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
    letterIdxBits = 6                    // 6 bits to represent a letter index
    letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
    letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)


func RandStringBytesMaskImprSrc(n int) string {
    b := make([]byte, n)
    // A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
    for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
        if remain == 0 {
            cache, remain = src.Int63(), letterIdxMax
        }
        if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
            b[i] = letterBytes[idx]
            i--
        }
        cache >>= letterIdxBits
        remain--
    }

    return string(b)
}

func KeyGen(G curve.G1Jac) Key {
	var key Key
	hFunc := sha256.New()

	key.seed = RandStringBytesMaskImprSrc(20)
	hFunc.Write([]byte(key.seed))
	key1 := hFunc.Sum(nil)

	keyI := new(big.Int)
	keyI.SetBytes(key1)
	var x fr.Element
	(&x).SetBigInt(keyI)
	key.PrivateKey=&x

	key.PublicKey.ScalarMultiplication(&G, keyI)

	return key
}

func GetPublicKeys(keys []Key) []curve.G1Jac{
	var res []curve.G1Jac
	for i := 0; i < len(keys); i++ {
		res = append(res, keys[i].PublicKey)
	}
	return res
}

func GetPrivateKeys(keys []Key) []*fr.Element{
	var res []*fr.Element
	for i := 0; i < len(keys); i++ {
		res = append(res, keys[i].PrivateKey)
	}
	return res
}

func Encrypt(t *big.Int, n *big.Int,  ephemeralSK *fr.Element, keys []curve.G1Jac, secret curve.G1Jac, G curve.G1Jac, GAff curve.G1Affine,  fft bool) ([]curve.G1Jac){
	cipher := make([]curve.G1Jac, int(n.Int64()))
	shares := make([]curve.G1Jac, int(n.Int64()))

	_, _,  shares, _ = shamir.CreatePoly(t, n , secret , GAff,  fft)

	for j := 0; j < int(n.Int64()); j++ {
		var E curve.G1Jac
		var bigk big.Int
		E.ScalarMultiplication(&keys[j], ephemeralSK.BigInt(&bigk))
		E.AddAssign(&shares[j])
		cipher[j] = E
	}
	return cipher
}


func DecShare(index int, cipher []curve.G1Jac, ephemeralPK curve.G1Jac, key *fr.Element) curve.G1Jac{
	var Res curve.G1Jac
	var bigk big.Int
	Res.ScalarMultiplication(&ephemeralPK, key.BigInt(&bigk)).Neg(&Res).AddAssign(&cipher[index])
	return Res
}


func ReshareNew(t *big.Int, n *big.Int, index int, cipher []curve.G1Jac, commonPublicKey curve.G1Jac, receiverKey *fr.Element, senderKey *fr.Element, NewReceiverKeys []curve.G1Jac, G curve.G1Jac, GAff curve.G1Affine, fft bool) ([]curve.G1Jac) {

	secret := DecShare(index, cipher , commonPublicKey, receiverKey)

	NewCipher := Encrypt(t, n, senderKey, NewReceiverKeys, secret, G, GAff, fft)

	return NewCipher
	}


func ReshareComb(n *big.Int, index []int, resharedDec [][]curve.G1Jac, senderKeys []curve.G1Jac, fft bool) ([]curve.G1Jac, curve.G1Jac) {
		var origin fr.Element
		origin.SetZero()

		coeffs := shamir.LagrangeCoefficients(n, index, origin,fft)

		resharedCipher := make([]curve.G1Jac, int(n.Int64()))
		var commonPublicKey curve.G1Jac


		for j := 0; j < int(n.Int64()); j++ {
			for i, c := range coeffs {
				var tmp curve.G1Jac
				var res big.Int
				c.ToBigIntRegular(&res)

				tmp.ScalarMultiplication(&resharedDec[i-1][j], &res)
				resharedCipher[j].AddAssign(&tmp)

				if j == 0 {
					var tmp curve.G1Jac
					tmp.ScalarMultiplication(&senderKeys[i-1], &res)
					commonPublicKey.AddAssign(&tmp)
				}
		}
		}

		return resharedCipher, commonPublicKey
	}



func Decrypt(n *big.Int, cipher []curve.G1Jac, commonPublicKey curve.G1Jac, PrivateKeys []*fr.Element, fft bool) (curve.G1Jac) {

	resharedDec := make([]curve.G1Jac, len(PrivateKeys))

	for j := 0; j < len(PrivateKeys); j++ {
		secret := DecShare(j, cipher , commonPublicKey, PrivateKeys[j])
		resharedDec[j]= secret
	}

	var origin fr.Element
	origin.SetZero()
	recReshared := shamir.InterpolLagrangeG1Jac(n, resharedDec, origin, fft)

	return recReshared

}
