package shamir

import (
	"math/big"
	//curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr/fft"

)


// FFTEvalRecur evaluates recursively the polynomial P at the n n-th root of unity in O(n.log(n)) using Cooley-Tukey algorithm
func FFTEvalRecur(P []fr.Element) []fr.Element {
	var n = len(P)
	if n == 1 {
		return []fr.Element{P[0]}
	}
	var P0 = make([]fr.Element, n/2)
	var P1 = make([]fr.Element, n/2)
	for i := 0; i < n; i++ {
		if i%2 == 0 {
			P0[i/2] = P[i]
		} else {
			P1[(i-1)/2] = P[i]
		}
	}
	var omega_n = &(fft.NewDomain(uint64(n)).Generator)
	var omega = fr.NewElement(1)

	var Y0 = FFTEvalRecur(P0)
	var Y1 = FFTEvalRecur(P1)
	var Y = make([]fr.Element, n)

	var somme fr.Element
	var product fr.Element
	for k := 0; k < n/2; k++ {
		product.Mul(&omega, &Y1[k])
		somme.Add(&Y0[k], &product)
		Y[k] = somme
		somme.Sub(&Y0[k], &product)
		Y[k+n/2] = somme
		product.Mul(&omega, omega_n)
		omega = product
	}
	return Y
}

// AddPadding adds padding to the slice 'coefficients' and returns a 'length'-long slice starting with coefficients
func AddPadding(coefficients []fr.Element, length uint64) []fr.Element {
	var newCoefficients = make([]fr.Element, length-uint64(len(coefficients)))
	return append(coefficients, newCoefficients...)
}



func ComputeCoefficientfft(n uint64) []fr.Element {
	var dom = fft.NewDomain(n)
	var omega_n = dom.Generator

	index := make([]fr.Element, n)
	var indice fr.Element
	for j := 0; j < int(n); j++ {
		indice.Exp(omega_n, big.NewInt(int64(j)))
		index[j] = indice
	}
	return index
}

/*
func InterpolLagrangeG1Jacfft(n *big.Int, y []curve.G1Jac) curve.G1Jac {
	var N = len(y) // == len(y)

	// Compute all points
	x := ComputeCoefficientfft(uint64(n.Int64()))

	var result curve.G1Jac

	var eval fr.Element
	eval.SetZero()

	for j := 0; j < N; j++ {
		lagrangeNumerator  := fr.One()
		lagrangeDenominator := fr.One()
		for i := 0; i < N; i++ {
			if i != j {
				var A fr.Element
				var B fr.Element
				A.Sub(&eval, &x[i])
				lagrangeNumerator.Mul(&lagrangeNumerator, &A)
				B.Sub(&x[j], &x[i])
				lagrangeDenominator.Mul(&lagrangeDenominator, &B)
			}
		}
		var v fr.Element
		v.Div(&lagrangeNumerator, &lagrangeDenominator)

		var tmp curve.G1Jac
		var res big.Int
		tmp.ScalarMultiplication(&y[j], v.ToBigIntRegular(&res))
		result.AddAssign(&tmp)
	}

	return result
}
*/


/*
func CreatePolyfft(t *big.Int, n *big.Int, C curve.G1Jac, GAff curve.G1Affine ) ([]fr.Element, []fr.Element, []curve.G1Jac) {

	pol := make([]fr.Element, int(t.Int64())+1)
	var origin fr.Element
	origin.SetZero()
	origin.FromMont()
	pol[0] = origin

  for i := 1; i < int(t.Int64())+1; i++ {
		var temp fr.Element
		temp.SetRandom()
		pol[i]= temp
  }

	//calculate shares, based on pol
	var dom = fft.NewDomain(uint64(n.Int64()))
	var card = dom.Cardinality

	var polPadded = AddPadding(pol, card)
	shares := FFTEvalRecur(polPadded)


	var sharesGroup []curve.G1Affine
	sharesGroupJac := make([]curve.G1Jac, int(n.Int64()))
	sharesGroup = curve.BatchScalarMultiplicationG1(&GAff, shares)

	//for i := 0; i < len(sharesGroup); i++ {
	for i := 0; i < int(n.Int64()); i++ {
		sharesGroupJac[i].FromAffine(&sharesGroup[i])
		sharesGroupJac[i].AddAssign(&C)
	}
	return  polPadded, shares,  sharesGroupJac
}
*/
