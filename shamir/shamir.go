package shamir

import (
	"math/big"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr/fft"

)

const pSize = 230

// Eval evaluates p at v
// returns a fr.Element
func Eval(p []fr.Element, v fr.Element) fr.Element {

	res := (p)[len(p)-1]

	for i := len(p) - 2; i >= 0; i-- {
		res.Mul(&res, &v)
		res.Add(&res, &(p)[i])
	}

	return res
}

// EvalfrBig evaluates p at v
// returns a *fr.Element
func EvalfrBig(p []fr.Element, v *big.Int) *fr.Element {

	var res big.Int
	var resfr fr.Element
	(&(p[len(p)-1])).BigInt(&res)
	var d big.Int
	for i := len(p) - 2; i >= 0; i-- {
		res.Mul(&res, v)
		res.Add(&res, (&(p[i])).BigInt(&d))
	}
	(&resfr).SetBigInt(&res)
	return &resfr
}

// EvalBig evaluates p at v
// returns a *big.Int
func EvalBig(p []big.Int, v *big.Int) *big.Int {
	var res big.Int
	(&res).Set(&(p[len(p)-1]))

	for i := len(p) - 2; i >= 0; i-- {
		res.Mul(&res, v)
		res.Add(&res, &(p[i]))
	}

	return &res
}

// CreatePolyElem calculates the secrets to share from given parameters
// t: degree
// n: number of shares
// p: size of finite field
// k: secret to share
func CreatePoly(t *big.Int, n *big.Int, C curve.G1Jac , GAff curve.G1Affine, FFT bool ) ([]fr.Element, []fr.Element, []curve.G1Jac, []curve.G1Jac) {

	pol := make([]fr.Element, int(t.Int64())+1)
	var origin fr.Element
	origin.SetZero()
	pol[0] = origin

  for i := 1; i < int(t.Int64())+1; i++ {
		var temp fr.Element
		temp.SetRandom() //.FromMont()
		//temp.FromMont()
		pol[i]= temp
  }

	//calculate shares, based on pol
	var shares []fr.Element
	var sharesGroup []curve.G1Affine
	sharesGroupJac := make([]curve.G1Jac, int(n.Int64()))
	EvalG := make([]curve.G1Jac, int(n.Int64()))

	if FFT == false {
		for i := 1; i < int(n.Int64())+1; i++ {
			// Evaluation Point
			var ePoint fr.Element
			ePoint.SetInt64(int64(i))

			eResult:= Eval(pol, ePoint)
			shares = append(shares, eResult)

			}
	} else {
		//calculate shares, based on pol
		var dom = fft.NewDomain(uint64(n.Int64()))
		var card = dom.Cardinality
		var polPadded = AddPadding(pol, card)
		sharesfft := FFTEvalRecur(polPadded)
		shares = sharesfft[:int(n.Int64())]
	}

	sharesGroup = curve.BatchScalarMultiplicationG1(&GAff, shares)

	for i := 0; i < len(sharesGroup); i++ {
		sharesGroupJac[i].FromAffine(&sharesGroup[i])
		EvalG[i].FromAffine(&sharesGroup[i])
		sharesGroupJac[i].AddAssign(&C)
	}
	return  pol, shares,  sharesGroupJac , EvalG
}



func InterpolLagrangeG1Jac(n *big.Int, y []curve.G1Jac, eval fr.Element, FFT bool) curve.G1Jac {
	var N = len(y)
	var x = make([]fr.Element, int(n.Int64()))

	if FFT == false {
		// Compute all coefficients
		for j := 0; j < N; j++ {
			x[j].SetInt64(int64(j+1))
		}
	} else {
		x = ComputeCoefficientfft(uint64(n.Int64()))
	}
	var result curve.G1Jac

	for j := 0; j < N; j++ {
		lagrangeNumerator  := fr.One()
		lagrangeDenominator := fr.One()
		for i := 0; i < N; i++ {
			if i != j {
				var A fr.Element
				var B fr.Element
				A.Sub(&eval, &x[i])
				lagrangeNumerator.Mul(&lagrangeNumerator, &A)
				B.Sub(&x[j], &x[i])
				//inv.Inverse(&inv)
				lagrangeDenominator.Mul(&lagrangeDenominator, &B)
			}
		}
		var v fr.Element
		v.Div(&lagrangeNumerator, &lagrangeDenominator)

		var tmp curve.G1Jac
		var res big.Int
		tmp.ScalarMultiplication(&y[j], v.ToBigIntRegular(&res))
		result.AddAssign(&tmp)
	}

	return result
}


func LagrangeCoefficients(n * big.Int, index []int, eval fr.Element, FFT bool) map[int]fr.Element {
	var N = len(index) // == len(y)
	var x = make([]fr.Element, int(n.Int64()))
	//var res = make([]fr.Element, n)

	res := make(map[int]fr.Element)

	if FFT == false {
		// Compute all coefficients
		for j := 0; j < N; j++ {
			x[j].SetInt64(int64(j+1))
		}
	} else {
		x = ComputeCoefficientfft(uint64(n.Int64()))
	}

	for j := 0; j < N; j++ {
		lagrangeNumerator  := fr.One()
		lagrangeDenominator := fr.One()
		for i := 0; i < N; i++ {
			if i != j {
				var A fr.Element
				var B fr.Element
				A.Sub(&eval, &x[i])
				lagrangeNumerator.Mul(&lagrangeNumerator, &A)
				B.Sub(&x[j], &x[i])
				//inv.Inverse(&inv)
				lagrangeDenominator.Mul(&lagrangeDenominator, &B)
			}
		}
		var v fr.Element
		v.Div(&lagrangeNumerator, &lagrangeDenominator)

		res[index[j]] = v

	}
	return res
}

func CreateIndex(t * big.Int) []int{
    index := make([]int, int(t.Int64())+1)
    for i := range index {
        index[i] = i+ 1
    }
    return index
}


/*
func CreatePolyTest(t *big.Int, n *big.Int, C curve.G1Jac , FFT bool ) ([]fr.Element, []fr.Element) {

	pol := make([]fr.Element, int(t.Int64())+1)
	var origin fr.Element
	origin.SetZero()
	origin.FromMont()
	pol[0] = origin

  for i := 1; i < int(t.Int64())+1; i++ {
		var temp fr.Element
		temp.SetRandom()
		pol[i]= temp
  }

	//calculate shares, based on pol
	var shares []fr.Element

	if FFT == false {
		for i := 1; i < int(n.Int64())+1; i++ {
			// Evaluation Point
			var ePoint fr.Element
			ePoint.SetInt64(int64(i))

			eResult:= Eval(pol, ePoint)
			eResult.ToMont()
			shares = append(shares, eResult)

			}
	} else {

		var dom = fft.NewDomain(uint64(n.Int64()))
		var card = dom.Cardinality
		var polPadded = AddPadding(pol, card)
		sharesfft := FFTEvalRecur(polPadded)
		shares = sharesfft[:int(n.Int64())]
	}

	return  pol, shares
}


// Eval evaluates p at v
// returns a fr.Element
func Eval(p []fr.Element, v fr.Element) fr.Element {

	res := (p)[len(p)-1]

	for i := len(p) - 2; i >= 0; i-- {
		res.Mul(&res, &v)
		res.Add(&res, &(p)[i])
	}

	return res
}


// eval returns p(point)
func EvalElemTest(p []fr.Element, point fr.Element) fr.Element  {
	var res fr.Element
	t := len(p)
	res.Mul(&p[1], &point).Add(&res, &p[0])
	//res = p[0] + (p[1] * point)
	var x_pow fr.Element
	x_pow = point

	if t <= 2 {
		return res
	}
	for i := 2; i < t-1; i++ {
		x_pow.Mul(&x_pow, &point)
		var c fr.Element
		res.Add(&res, c.Mul(&p[i], &x_pow))

	}
	return res
}

*/
