package zk


import (

	"APSS/cryptofr"
	"APSS/shamir"
	"math/big"
	"github.com/consensys/gnark-crypto/ecc"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
)

//same as ComputeChallengeF
func ComputeChallengeH(data ProofData, n *big.Int, m *big.Int, X1 [][]curve.G1Affine, X2 [][]curve.G1Affine, rdPK curve.G1Affine , X1Star []curve.G1Affine, X2Star []curve.G1Affine, rdPKStar  curve.G1Affine, receiverPublicKeys []curve.G1Jac) *big.Int {
	c:=  new(big.Int)
	data.H.Reset()
	defer data.H.Reset()
	N:=int(n.Int64())
	M:=int(m.Int64())
	for i:=0; i<M;i++{
		for j := 0; j < N; j++ {
			data.H.Write([]byte(X1[i][j].String() ))
		}
		for j := 0; j < N; j++ {
			data.H.Write([]byte(X2[i][j].String() ))
		}
	}
	data.H.Write([]byte(rdPK.String() ))


	for j := 0; j < N; j++ {
		data.H.Write([]byte(X1Star[j].String() ))
	}
	for j := 0; j < N; j++ {
		data.H.Write([]byte(X2Star[j].String() ))
	}
		data.H.Write([]byte(rdPKStar.String() ))

	for j := 0; j < N; j++ {
			data.H.Write([]byte(receiverPublicKeys[j].String() ))
	}
	return c.SetBytes(data.H.Sum(nil))
}

//Given (z_i=(rd_i,PrivateKey_i,pol_i))_i and z*=(rd*,rdSK*,pol*), computes z*+sum_i c^i z_i
func ComputeStatementH(t *big.Int, n *big.Int, m *big.Int, c *big.Int, rd [][]*fr.Element, rdStar []*fr.Element, rdSKStar *fr.Element, PrivateKey *fr.Element, polStar []fr.Element, pol [][]fr.Element)([]*fr.Element, []fr.Element) {

	z1 := make([]*fr.Element, int(n.Int64()) +1)
	z2 := make([]fr.Element, int(t.Int64()) +1)
	rdj := make([]fr.Element,int(m.Int64()))
	var cc fr.Element
	var sumc, cfr fr.Element
	(&cfr).SetBigInt(c)
	// Computation of z1[j]= rdStar[j] +sum_l (c^l . rd_l[j] )
	for j := 0; j < int(n.Int64()); j++{
		for i:=0; i<int(m.Int64()); i++{
			(&rdj[i]).Set(rd[i][j])
		}
		var d1 fr.Element
		d1=shamir.Eval(rdj,cfr)
		(&d1).Mul(&d1,&cfr)
		d1.Add(rdStar[j], &d1)
		z1[j]=&d1
	}
	(&cc).Set(PrivateKey) // (powers of c)*PrivateKey
	(&sumc).SetZero() // sum of (powers of c)*PrivateKey
	for i:=0; i<int(m.Int64()); i++{
	cc.Mul(&cc,&cfr)
	sumc.Add(&cc, &sumc)
	}

	sumc.Add((&sumc),rdSKStar)
	z1[int(n.Int64())]=&sumc
	// Computation of z2[j]= (polStar[j] + sum_l c^l.pol_l[j] )

	polj:=make([]fr.Element,int(m.Int64()))
	for j := 0; j < int(t.Int64()) +1; j++ {
		for i:=0; i < int(m.Int64()); i++{
			polj[i]=pol[i][j]
		}
		var d4 fr.Element
		d4=shamir.Eval(polj, cfr)
		(&d4).Mul(&d4,&cfr)
		var temp fr.Element
		temp.Add(&polStar[j], &d4)
		z2[j] = temp
	}
	return z1, z2
}

// Given n,r, produces slice [1,r,r^2,...,r^n]
func ComputePowersH (n int, r *fr.Element)([] fr.Element){
	vecr:=make([] fr.Element, n+1)
	var rr fr.Element
	(&rr).SetOne()
	for j:=0; j<n+1; j++{
		vecr[j]=rr
		(&rr).Mul(&rr,r)
	}	
	return vecr
}

//same as ComputeFunctionF
func ComputeFunctionH(n *big.Int ,receiverPublicKeys []curve.G1Jac, Eval []curve.G1Jac, rd []*fr.Element, rdSK *fr.Element, C1 curve.G1Jac,  C2 curve.G1Jac, G curve.G1Jac) ([]curve.G1Jac, []curve.G1Jac, curve.G1Jac ) {

	cipherProof := make([]cryptofr.CipherMR, int(n.Int64()))
	var v big.Int
	for j := 0; j < int(n.Int64()); j++ {
		var C curve.G1Jac
		C.ScalarMultiplication(&receiverPublicKeys[j], rd[j].BigInt(&v))
		C.AddAssign(&Eval[j])
		cipherProof[j].C1 = C
		var E curve.G1Jac
		E.ScalarMultiplication(&G, rd[j].BigInt(&v))
		cipherProof[j].C2 = E
	}

	X1 := make([]curve.G1Jac, int(n.Int64()))
	var X2 []curve.G1Jac

	var Secret curve.G1Jac
	Secret.ScalarMultiplication(&C2, rdSK.BigInt(&v)).Neg(&Secret) //.AddAssign(&C1)

	for j := 0; j < int(n.Int64()); j++ {
		var temp curve.G1Jac
		temp = Secret
		temp.AddAssign(&cipherProof[j].C1)
		X1[j]= temp
	}

	for i := 0; i < len(cipherProof); i++ {
		X2 = append(X2, cipherProof[i].C2)
	}

	var rdPK curve.G1Jac
	rdPK.ScalarMultiplication(&G, rdSK.BigInt(&v))


	return X1, X2, rdPK
}


//precomputes scalars vecr=[1,r^1,...,r^{b(2n+1)}], vecrj=sum_{j=1}^n r^j[j^1,...,j^t], sumj=sum_{j=1...n}r^j
func PrecomputeScalarsH(n *big.Int, t *big.Int, b *big.Int)(*fr.Element, [] fr.Element, []fr.Element, fr.Element){
	N:=int(n.Int64())
	B:=int(b.Int64())
	T:=int(t.Int64())
	var r fr.Element
	(&r).SetRandom()

	vecr:=ComputePowersH(B*(2*N+1),&r)//[1,r,r^2,...,r^{b(2n+1)}]
	var sumj fr.Element //sum_{j=1...n} r^j
	vecrj:=make([]fr.Element,T) //sum_{j=1}^n r^j[j,...,j^t]
	
	for j:=0; j<N; j++{
		(&sumj).Add(&sumj,&vecr[j+1])	
		var jfr fr.Element
		(&jfr).SetInt64(int64(j+1))
		vecj:=ComputePowersH(T,&jfr)[1:] // [j,...,j^t]
		rj:=vecr[j+1] //r^j
			for i:=range(vecj){
				(&vecj[i]).Mul(&rj,(&vecj[i])) // vecj[i]=r^j.j^i .ToMont()
				(&vecrj[i]).Add(&vecrj[i],&vecj[i])
			}
	}
	
	return &r, vecr, vecrj, sumj	
}

//Given vecr=[1,r,...,r^p] and c, computes the vector [r,...,r^p,cr,...,cr^p,...,c^mr^p] for i=0...m and j=1...p
func ComputeScalarsVec (n *big.Int, t *big.Int, m *big.Int,  c *big.Int, vecr []fr.Element) []fr.Element{
	var cfr fr.Element
	(&cfr).SetBigInt(c)
	vecc:=ComputePowersH(int(m.Int64()),&cfr)
	M:=vecc
	P:=vecr
	var L []fr.Element
	var rc fr.Element
	for a:=range(M){
		for j:=1;j<len(P);j++{
			(&rc).Mul(&M[a],&P[j])
			L=append(L,rc)
		}
	}
	return L
}

//Computes the vector of all points of X [][][]curve.G1Jac : X*_0||X_0[0]||...||X_0[m-1]||X*_1||X_1[1]||.....||X_b[m]
func ComputePointsVec(n *big.Int, t *big.Int, m []*big.Int, X1 [][][]curve.G1Affine, X2 [][][]curve.G1Affine, rdPK []curve.G1Affine, X1Star [][]curve.G1Affine, X2Star [][]curve.G1Affine, rdPKStar []curve.G1Affine) []curve.G1Affine {
	var L []curve.G1Affine
	for a:=range(X1){
		L=append(L,X1Star[a]...)
		L=append(L,X2Star[a]...)
		L=append(L,rdPKStar[a])	
		for i:=0; i<int(m[a].Int64()); i++{
			L=append(L,X1[a][i]...)
			L=append(L,X2[a][i]...)
			L=append(L,rdPK[a])
		}
	}						
	return L						
}



//Computes, given w, a linear combination of coefficients of f(w)
// The scalars r, vecr=[1,r,...,r^{b(2n+1)}, vecrj =sum_{j=1}^n r^j[j^1...j^t], sumj=sum_{j=1}^n r^j are supposed to be given by PrecomputeScalarsH
func ComputeFunctionVerificationH(n *big.Int, t *big.Int, b *big.Int, receiverPublicKeys [][]curve.G1Jac, pol [][]fr.Element, rd [][]*fr.Element, rdSK []*fr.Element, C1 []curve.G1Jac,  C2 []curve.G1Jac, G curve.G1Jac, r *fr.Element, vecr []fr.Element, vecrj []fr.Element, sumj fr.Element) (curve.G1Jac) {
		
	config:=ecc.MultiExpConfig{}
	// Preparing scalars
	N:=int(n.Int64())
	B:=int(b.Int64())
	T:=int(t.Int64())
	
	var sra fr.Element // r^{a(2n+1)+j}.rdSK[a]
	L1:=make([]fr.Element,B) // [-(sum_j r^{a(2n+1)+j} rdSK[a]]), a=0...b-1
	var L2 [] fr.Element // [r^{a(2n+1)+j}.rd[a][j]], a=1...B,j=1...N
		
	var sum1 fr.Element // sum_{a,j} r^a(2n+1).r^j.pol[a](j)
	var sum2 fr.Element // sum_{a,j} r^a(2n+1).r^{n+j}.rd[a][j]
	var sum3 fr.Element // sum_a r^{(a+1)(2n+1)} rdSK[a]
	var sum fr.Element // sum1+sum2+sum3
	var dfr fr.Element // helper variable
	var dint big.Int // helper variable
	
	sumpol := make([]fr.Element,len(pol[0])) //sum_{a=0...b-1}r^{a(2n+1)}pol[a]
	
	LPts:=C2 // this will be C2 appended with [receiverPublicKeys[a][j]], a=0...B-1, j=0...N-1	
	for a:=0; a<B; a++{
		indexa:=a*(2*N+1)
		rrr:=vecr[indexa] // r^{a(2n+1)}
		(&sra).Mul(&rrr,rdSK[a]) //sra = r^{a(2n+1)}rdSK[a]
		(&L1[a]).Mul(&sra,&sumj) 
		(&L1[a]).Neg(&L1[a]) // L1[a]=-(sum_{j=1}^n r^{a(2n+1)+j)rdSK[a]
		
		(&dfr).Mul(&sra,&vecr[2*N+1]) //
		(&sum3).Add(&dfr,&sum3) // sum3 = sum_a r^{(a+1)(2n+1)}
		
		for i:=0; i<T; i++{
			(&dfr).Mul((&rrr),&pol[a][i+1])
			(&sumpol[i]).Add(&sumpol[i],&dfr)
		} // now sumpol[i] = sum_a r^{a(2n+1)}pol[a][i]
		
		LPts=append(LPts,receiverPublicKeys[a]...)

		for j:=0; j<N;j++{
			index:=indexa+j+1 // a(2n+1)+j
			sarj:=vecr[index] // r^{a(2n+1)+j}
			(&dfr).Mul(&sarj,rd[a][j]) // r^{a(2n+1)+j}.rd[a][j]
			L2 = append(L2, dfr)
			(&dfr).Mul(&dfr,&vecr[N]) // r^{a(2n+1)+n+j}.rd[a][j]
			(&sum2).Add(&sum2,&dfr)
		}
	}
	
	for i:=range(vecrj){
		(&dfr).Mul(&sumpol[i],&vecrj[i])
		(&sum1).Add(&sum1,&dfr)
	} //sum1= sum_{a,j}r^{a(2n+1)}r^j.pol[a][j]
	(&sum).Add(&sum1,&sum2)
	(&sum).Add(&sum,&sum3)
	L2 = append(L1,L2...)

	var X curve.G1Jac //the result
	var D curve.G1Jac //helper variable
	
	(&X).MultiExp(curve.BatchJacobianToAffineG1(LPts),L2,config)
	(&D).ScalarMultiplication(&G,(&sum).BigInt(&dint))
	(&X).AddAssign(&D)

	return X
}

// Given c, X, XStar, computes X*[a]+sum_i c[a]^i X[a][i]
func ComputeLinCombH(n *big.Int, m []*big.Int, X1 [][][]curve.G1Affine, X2 [][][]curve.G1Affine, rdPK []curve.G1Affine, X1Star [][]curve.G1Affine, X2Star [][]curve.G1Affine, rdPKStar []curve.G1Affine, c []*big.Int)([][]curve.G1Affine, [][]curve.G1Affine, []curve.G1Affine){
	N:=int(n.Int64())
	B:=len(X1)
	config:=ecc.MultiExpConfig{}
	Y1 := make([][]curve.G1Affine, B) 
	Y2 := make([][]curve.G1Affine, B)
	rdPKY:=make([]curve.G1Affine, B)
	var d curve.G1Affine

	for a:=range(X1){
		M:=int(m[a].Int64())
		var cfr fr.Element
		(&cfr).SetBigInt(c[a])
		vecc:=ComputePowersH(M,&cfr)[1:]
		L:=make([]curve.G1Affine, M)
		Xj1:=make([]curve.G1Affine, M)
		Xj2:=make([]curve.G1Affine, M)
		for  i:=range Xj1{
			L[i]=rdPK[a]
		}
		for j:=0; j<N; j++{
			for i:=range(Xj1){ 		
					Xj1[i]=X1[a][i][j]
					Xj2[i]=X2[a][i][j]
			}	

			(&d).MultiExp(Xj1,vecc,config)
			(&d).Add(&d,&X1Star[a][j])
			Y1[a]=append(Y1[a],d)
			(&d).MultiExp(Xj2,vecc,config)
			(&d).Add(&d,&X2Star[a][j])
			Y2[a]=append(Y2[a],d)
		}				
		(&rdPKY[a]).MultiExp(L,vecc,config)
		(&rdPKY[a]).Add(&rdPKY[a],&rdPKStar[a]) 
		}
		 
	return Y1, Y2, rdPKY	
}


//Given witnesses w_i=(rd_i,pol_i), computes random w*, and returns X*=f(w*) and proof z=w*+sum_ic^i.w_i 
func ComputeProofH(dataProof ProofData, t *big.Int, n *big.Int, b *big.Int, m []*big.Int,  inputCipher []cryptofr.CipherMR, receiverPublicKeys [][]curve.G1Jac, rd [][][]*fr.Element, pol [][][]fr.Element,
							cipher [][][]cryptofr.CipherMR , PrivateKey []*fr.Element , X1 [][][]curve.G1Affine, X2 [][][]curve.G1Affine, rdPK []curve.G1Affine , G curve.G1Jac,
							GAff curve.G1Affine, FFT bool, RandomChallenge bool) ([]*big.Int, [][]*fr.Element, [][]fr.Element, [][]curve.G1Affine, [][]curve.G1Affine, []curve.G1Affine){

	N:=int(n.Int64())
	B:=int(b.Int64())
	rdStar := make([][]*fr.Element, B)
	polStar := make([][]fr.Element,B)
	EvalStar := make([][]curve.G1Jac,B)
	X1Star := make([][]curve.G1Affine, B)
	X2Star := make([][]curve.G1Affine, B)
	rdSKStar := make ([]*fr.Element, B)
	rdPKStar := make ([]curve.G1Affine, B)
	z1 := make([][]*fr.Element, B)
	z2 := make([][]fr.Element, B)


	c := make([] *big.Int, B)
	for a:=range(m){
		// 1. Generate randomness rdStar_a
		for j := 0; j < N; j++ {
			var rnd fr.Element
			(&rnd).SetRandom()
			rdStar[a]=append(rdStar[a],&rnd)
		}
	
		// 2. Generate random polynomial polStar_a
		var C curve.G1Jac
		C.X.SetOne()
		C.Y.SetOne()
		C.Z.SetZero()
		polStar[a], _, EvalStar[a], _ = shamir.CreatePoly(t, n , C, GAff, FFT)
		
		// 3. Generate random secret key rdSKStar_a
		u:=GenerateRandomness(dataProof.H)
		var uu fr.Element
		(&uu).SetBigInt(u)
		rdSKStar[a] = &uu

		// 4. Compute XStar_a = f(wStar_a)
		UA,VA,WA:= ComputeFunctionH(n, receiverPublicKeys[a], EvalStar[a], rdStar[a], rdSKStar[a], inputCipher[a].C1, inputCipher[a].C2, G)
		X1Star[a]=curve.BatchJacobianToAffineG1(UA)
		X2Star[a]=curve.BatchJacobianToAffineG1(VA)
		(&rdPKStar[a]).FromJacobian(&WA)
		// 5. Compute challenge c_a=H(X_{a,1}...X_{a,m_a},Xstar_a)
		if (RandomChallenge == true){
			c[a] = GenerateRandomness(dataProof.H)
		} else{
			c[a] = ComputeChallengeH(dataProof, n, m[a], X1[a], X2[a], rdPK[a], X1Star[a], X2Star[a], rdPKStar[a], receiverPublicKeys[a])
		}
		// 6. Compute proof
		z1[a], z2[a] = ComputeStatementH(t, n, m[a], c[a], rd[a], rdStar[a], rdSKStar[a], PrivateKey[a], polStar[a], pol[a])
		}
		

	return c, z1, z2, X1Star, X2Star, rdPKStar
}
	

func VerifyProofH(dataProof ProofData, n *big.Int, t *big.Int, b *big.Int, m []*big.Int,  c []*big.Int, z1 [][]*fr.Element, z2 [][]fr.Element, X1 [][][]curve.G1Affine, X2 [][][]curve.G1Affine, rdPK []curve.G1Affine, X1Star [][]curve.G1Affine, X2Star [][]curve.G1Affine, rdPKStar []curve.G1Affine, LP []curve.G1Affine,
										receiverPublicKeys [][]curve.G1Jac, C1 []curve.G1Jac, C2 []curve.G1Jac, G curve.G1Jac, r *fr.Element, vecr []fr.Element, vecrj []fr.Element, sumj fr.Element, RandomChallenge bool) (bool, bool) {

	N := int(n.Int64())
	B := int(b.Int64())
	rd := make([][] *fr.Element,B)
	rdSK := make([] *fr.Element,B)
	pol := make([][] fr.Element,B)
	e := make([]*big.Int,B)
	
	//1. Parse \pi	
	for a:=range(m){	
	rd[a] = z1[a][:N]
	rdSK[a] = z1[a][N]
	pol[a] = z2[a]
	}
	

	if RandomChallenge ==true{
		e=c
	} else{
		for a:=range(m){
		e[a]=ComputeChallengeH(dataProof, n, m[a], X1[a], X2[a], rdPK[a], X1Star[a], X2Star[a], rdPKStar[a], receiverPublicKeys[a])
		}
	}




	
	// 2. Compute linear combination of coordinates of f(z) with random coefficients
	XZ := ComputeFunctionVerificationH(n, t, b, receiverPublicKeys, pol, rd, rdSK, C1,  C2, G, r, vecr, vecrj, sumj)
	


	
	
	var sumY curve.G1Jac

		
	
/*	//3.Variant1. Compute X*+sum_i c^iX_i for each a, then recombine 	
	Y1,Y2,rdPKY:=ComputeLinCombH(n, m, X1, X2, rdPK, X1Star, X2Star, rdPKStar,c)
	var vecY []curve.G1Affine
	config:=ecc.MultiExpConfig{}

	for a:=range(Y1){
		vecY=append(vecY,Y1[a]...)
		vecY=append(vecY,Y2[a]...)
		vecY=append(vecY,rdPKY[a])
	}
	sumY.MultiExp(vecY,vecr[1:],config)
*/	
	//3.Variant2. Compute one big MultiExp with everything
	
	var LS []fr.Element
	for a:=range(X1){
	LS=append(LS,ComputeScalarsVec(n,t,m[a],c[a],vecr[a*(2*N+1):(a+1)*(2*N+1)+1])...)
	}


	sumY.MultiExp(LP,LS,ecc.MultiExpConfig{})
		




	
	// 5. Check f(z)=XStar+cX
		
	var result1 bool = (&XZ).Equal(&sumY) 
	var result2 bool = true
	for a:=0; a<B;a++{
	result2 = result2 &&(e[a].Cmp(c[a]) == 0)
	}
	return result1,result2
}


