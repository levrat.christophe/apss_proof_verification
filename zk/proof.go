package zk

import (
	"fmt"
	"crypto/sha256"
	"hash"
	"strings"
	"APSS/cryptofr"
	"APSS/shamir"
	"strconv"
	"math/big"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
)


var g1Jac, g2Jac, g1Aff, g2Aff = curve.Generators()


func main() {
	data := "test"
	hFunc := sha256.New()
	hFunc.Write([]byte(data))
	hFunc.Write([]byte("1"))
	key1 := hFunc.Sum(nil)
	fmt.Printf("hash 1: %v \n", key1)

}


type Transcript struct {
    Keys   curve.G1Jac
    Cipher  curve.G1Jac
}

type Proof struct {
	U curve.G1Jac
	V curve.G1Jac
	W curve.G1Jac
	C *big.Int
	Z1 big.Int
	Z2 big.Int
}


type ProofData struct {
	H hash.Hash
	CommonPublicKey  curve.G1Jac
	Cipher []curve.G1Jac
}


func Zip(tk []cryptofr.Key, tc []curve.G1Jac) []Transcript {
    if len(tk) != len(tc) {
        panic("not same length")
    }
    var res []Transcript
    for i, t := range tk {
        res = append(res, Transcript{Keys: t.PublicKey, Cipher: tc[i]})
    }
    return res
}



func GenerateRandomness(hFunc hash.Hash) *big.Int {
	rd := cryptofr.RandStringBytesMaskImprSrc(20)
	hFunc.Write([]byte(rd))
	k := hFunc.Sum(nil)
	kI := new(big.Int)
	kI.SetBytes(k)
	return kI
}



func ProofSetup(degree int, fft bool) ([]fr.Element, []fr.Element) {
	points := make([]fr.Element, degree +1)
	v := make([]fr.Element, degree +1)

	var origin fr.Element
	origin.SetZero()

	if fft == false {
		// Compute all coefficients
		for j := 0; j < degree +1; j++ {
			points[j].SetInt64(int64(j))
		}
	} else {
		coeffs := shamir.ComputeCoefficientfft(uint64(degree))
		points = append([]fr.Element{origin}, coeffs...)
	}

	//compute v_i
	for i := 0; i < degree +1 ; i++ {
		Denominator := fr.One()
		for j := 0; j < degree +1; j++ {
		// skip the case when i = j
			if i == j {
				continue
				}
				var tempD fr.Element
				tempD.Sub(&points[i], &points[j])
				Denominator.Mul(&Denominator, &tempD)
		}
		var l fr.Element
		l.Inverse(&Denominator)
		v[i] = l
	}
	return points, v
}


func ComputeStatement(mStar []fr.Element, points []fr.Element, v []fr.Element,
											commonPublicKey curve.G1Jac, inputCipher curve.G1Jac,  resharedCipher []curve.G1Jac, NewReceiverKeys []curve.G1Jac, N int) (curve.G1Jac, curve.G1Jac, curve.G1Jac) {


	var U curve.G1Jac
	var V curve.G1Jac
	var W curve.G1Jac

	var IntermW big.Int

	for j := 0; j < N; j++ {
		var c fr.Element
		eval := shamir.Eval(mStar, points[j])
		c.Mul(&v[j], &eval)
		var res big.Int
		c.BigInt(&res)

		resharedCipher[j].SubAssign(&inputCipher)

		var IntermU curve.G1Jac
		var IntermV curve.G1Jac
		IntermU.ScalarMultiplication(&resharedCipher[j], &res)
		IntermV.ScalarMultiplication(&NewReceiverKeys[j], &res)

		U.AddAssign(&IntermU)
		V.AddAssign(&IntermV)

		IntermW.Add(&IntermW, &res)

	}

	W.ScalarMultiplication(&commonPublicKey, &IntermW)

	return U, V , W
}


// ComputeCoefficient computes the coefficients corresponding to the given name.
// The challenge is:
// * H(proof data || '1') if it's is the first challenge
// * H(proof data || '1' || ... ||'t') if it's is the t-th challenge
func ComputeCoefficient(data ProofData, degree int) ([]fr.Element, error) {

	coeffs := make([]fr.Element, degree+1)
	// reset before populating the internal state
	data.H.Reset()
	defer data.H.Reset()

	// write the proof data as string
	dataStr := ProofDataString(data)

	if _, err := data.H.Write([]byte(dataStr)); err != nil {
		return nil, err
	}

	for i :=0 ; i <= degree; i++{
		coeffs[i].SetBytes(data.H.Sum(nil))
		if _, err := data.H.Write([]byte(strconv.Itoa(i+1))); err != nil {
			return nil, err
		}
	}
	return coeffs, nil
}


func ProofDataString(data ProofData) string{
	var sb strings.Builder

	sb.WriteString(data.CommonPublicKey.String())
		for i :=0 ; i < len(data.Cipher); i++{
		sb.WriteString(data.Cipher[i].String())
	}
	return sb.String()
}


// Challange of the form H(pkd, U, V, A, B)
func ComputeChallenge(data ProofData, f1R curve.G1Jac , f2R curve.G1Jac ,f3R curve.G1Jac , f1 curve.G1Jac , f2 curve.G1Jac ,f3 curve.G1Jac ) *big.Int {
	c:=  new(big.Int)


	data.H.Reset()
	defer data.H.Reset()

	data.H.Write([]byte(f1R.String() ))
	data.H.Write([]byte(f2R.String() ))
	data.H.Write([]byte(f3R.String() ))
	data.H.Write([]byte(f1.String() ))
	data.H.Write([]byte(f2.String() ))
	data.H.Write([]byte(f3.String() ))

	hashB := data.H.Sum(nil)
	c.SetBytes(hashB)

	return c

}


func ComputeFunctionProof(r1 *big.Int, r2 *big.Int, V curve.G1Jac, W curve.G1Jac, G curve.G1Jac) (curve.G1Jac, curve.G1Jac, curve.G1Jac) {
	var f1 curve.G1Jac
	var f2 curve.G1Jac
	var f3 curve.G1Jac

	f1.ScalarMultiplication(&G, r1)
	f2.ScalarMultiplication(&G, r2)

	var interm1 curve.G1Jac
	var interm2 curve.G1Jac

	interm1.ScalarMultiplication(&V, r2)
	interm2.ScalarMultiplication(&W, r1)
	f3.AddAssign(&interm1)
	f3.SubAssign(&interm2)

	return f1, f2, f3
}

func Prove(dataProof ProofData, U curve.G1Jac, V curve.G1Jac, W curve.G1Jac, receiverPublicKey curve.G1Jac, senderPublicKey curve.G1Jac,
							receiverPrivateKey *big.Int, senderPrivateKey *big.Int, G curve.G1Jac) (*big.Int, big.Int , big.Int ){

	r1 := GenerateRandomness(dataProof.H)
	r2 := GenerateRandomness(dataProof.H)

	f1, f2, f3 := ComputeFunctionProof(r1, r2, V, W, G)

	// Compute challenge

	c := ComputeChallenge(dataProof, receiverPublicKey, senderPublicKey, U, f1, f2, f3)

	// Compute
	var z1 big.Int
	var z2 big.Int

	var d1 big.Int
	var d2 big.Int

	d1.Mul(c,receiverPrivateKey )
	d2.Mul(c,senderPrivateKey )

	z1.Add(r1, &d1)
	z2.Add(r2, &d2)

	return c, z1, z2
}

func ProveVerify(dataProof ProofData,  U curve.G1Jac, receiverPublicKey curve.G1Jac, senderPublicKey curve.G1Jac,  V curve.G1Jac, W curve.G1Jac,
								c *big.Int, z1 big.Int, z2 big.Int , G curve.G1Jac, ch chan bool) bool {


	f1V, f2V, f3V := ComputeFunctionProof(&z1, &z2, V, W, G)

	var b1 curve.G1Jac
	var b2 curve.G1Jac
	var b3 curve.G1Jac

	b1.ScalarMultiplication(&receiverPublicKey, c)
	b2.ScalarMultiplication(&senderPublicKey, c)
	b3.ScalarMultiplication(&U, c)

	f1V.SubAssign(&b1)
	f2V.SubAssign(&b2)
	f3V.SubAssign(&b3)

	ch <- true
	e := ComputeChallenge(dataProof, receiverPublicKey, senderPublicKey, U, f1V, f2V, f3V)
	<- ch
	var result bool = e.Cmp(c) == 0
	return result
}
