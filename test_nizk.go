package main

import (
	"fmt"
	"math/big"
	"crypto/sha256"
	"APSS/zk"
	"APSS/cryptofr"
	"time"
	"runtime"
	curve "github.com/consensys/gnark-crypto/ecc/bn254"
	"github.com/consensys/gnark-crypto/ecc/bn254/fr"
)


func main() {
	tint :=25
	Wint :=5
	nint:= 2*tint+1
	kappa := tint+1
	t := big.NewInt(int64(tint))
	n:=big.NewInt(int64(nint))
	W:= big.NewInt(int64(Wint))
	b:=big.NewInt(int64(kappa))
	fft := false
	


	numCPUs := runtime.NumCPU()
	fmt.Printf("cpu : %v\n", numCPUs)

  runtime.GOMAXPROCS(2)
	fmt.Printf("cores : %v\n", runtime.GOMAXPROCS(2))

	fmt.Printf("goroutines : %v\n", runtime.NumGoroutine())


	fmt.Printf("t : %v, n: %v, b: %v, W:%v\n", t, n, b, W)

	fmt.Println("Length of multiexponentiation in verification: (m+1)*(2n+1)*(t+1) = ", (2*nint+1)*kappa*(Wint+1))

	
	r1,r2:=test_NIZKVerifyfinal(n,t,b,W,1,fft,false)
	fmt.Println("Output of verification:", r1&&r2)
  }


func benchmarkN(benchmarkOne func(n *big.Int, t *big.Int, indice int, fft bool, multi bool) time.Duration, n *big.Int, t *big.Int, indice int,  repeat int, fft bool, multi bool) float64 {
  var sum float64

  for i := 0; i < repeat; i++ {
  	sum += benchmarkOne(n, t, indice , fft, multi).Seconds()
  }
  return sum / float64(repeat) * 1000.0 // millisecndes
}

func benchmarkNseveral(benchmarkOne func(n *big.Int, t *big.Int, W *big.Int, indice int, fft bool, multi bool) time.Duration, n *big.Int, t *big.Int, W *big.Int, indice int,  repeat int, fft bool, multi bool) float64 {
  var sum float64

  for i := 0; i < repeat; i++ {
  	sum += benchmarkOne(n, t, W, indice , fft, multi).Seconds()
  }
  return sum / float64(repeat) * 1000.0 // millisecndes
}

func benchmarkNfinal(benchmarkOne func(n *big.Int, t *big.Int, b *big.Int, W *big.Int, indice int, fft bool, multi bool) time.Duration, n *big.Int, t *big.Int, b *big.Int, W *big.Int, indice int,  repeat int, fft bool, multi bool) float64 {
  var sum float64

  for i := 0; i < repeat; i++ {
  	sum += benchmarkOne(n, t, b, W, indice , fft, multi).Seconds()
  }
  return sum / float64(repeat) * 1000.0 // millisecndes
}

func ToBigIntSlice (F []fr.Element) []big.Int{
	L:=make([]big.Int, len(F))
	for a:=range(F){
			(&F[a]).BigInt(&L[a])
	}
	return L
}

func ToBigIntSlicePoint (F []*fr.Element) []*big.Int{
	var L []*big.Int
	for a:=range(F){
			var x big.Int
			(F[a]).BigInt(&x)
			L=append(L,&x)
	}
	return L
}


func benchmark_NIZKVerify_optimised(n *big.Int, t *big.Int, b *big.Int, W *big.Int, indice int, fft bool, multi bool) time.Duration {
	RandomChallenge := true
	B:=int(b.Int64())
	index := make([]int,  int(t.Int64()) +1)
	mm:=make([]*big.Int,B)
	for i:=range(mm){
		mm[i]=W
	}
	for j := 0; j < int(t.Int64()) +1; j++ {
		index[j] = j+1
	}
	
	var instance []*cryptofr.CommitteeSharingInstance
	var receiverPublicKeys [][]curve.G1Jac
	var receiverPrivateKeys [][]*fr.Element
	var G curve.G1Jac
	var GAff curve.G1Affine
	
	for a:=0; a<B; a++{
	instance = append(instance,cryptofr.NewCommitteeSharingInstance(t, n ,index, fft))
	receiverPublicKeys = append(receiverPublicKeys, cryptofr.GetPublicKeys(instance[a].ReceiverKeys))
	receiverPrivateKeys = append(receiverPrivateKeys, cryptofr.GetPrivateKeys(instance[a].ReceiverKeys))
	G = instance[a].G
	GAff = instance[a].GAff
	}

	var M fr.Element
	var S curve.G1Jac
	var MInt big.Int
	var msg int64
	var dataProof zk.ProofData
	dataProof.H = sha256.New()
		
	msg=int64(7)
	M.SetInt64(msg) //.FromMont()
	M.BigInt(&MInt)
	cipher:=make([][]cryptofr.CipherMR,B)
	cipherindice:=make([]cryptofr.CipherMR,B)
	cipherindice1:=make([]curve.G1Jac, B)
	cipherindice2:=make([]curve.G1Jac, B)
	receiverPrivateKeysindice:=make([]*fr.Element, B)
	for a:=0; a<B; a++{
	S.ScalarMultiplication(&G, &MInt)
	cipher[a], _, _, _ = cryptofr.EncryptMR(dataProof.H, t , n,  receiverPublicKeys[a],  S, G, GAff, fft, multi)
	cipherindice[a]=cipher[a][indice]
	cipherindice1[a]=cipherindice[a].C1
	cipherindice2[a]=cipherindice[a].C2
	receiverPrivateKeysindice[a]=receiverPrivateKeys[a][indice]
	}


	resharedCipher:=make([][][]cryptofr.CipherMR,B)
	rd:=make([][][]*fr.Element,B)
	polEnc:=make([][][]fr.Element,B)
	Eval:=make([][][]curve.G1Jac,B)
	X1:=make([][][]curve.G1Affine,B)
	X2:=make([][][]curve.G1Affine,B)
	rdPKEnc := make([]curve.G1Affine,B)
	
		Wint:=int(W.Int64())
		resharedCiphera:=make([][]cryptofr.CipherMR,Wint)
		rda:=make([][]*fr.Element,Wint)
		polEnca:=make([][]fr.Element,Wint)
		Evala:=make([][]curve.G1Jac,Wint)
		X1a:=make([][]curve.G1Affine,Wint)
		X2a:=make([][]curve.G1Affine,Wint)
		var rdPKEnca curve.G1Affine
	
		
		for i:=0; i<Wint; i++{
		resharedCiphera[i], rda[i], polEnca[i], Evala[i] = cryptofr.ReshareMR(dataProof.H, t , n, indice, cipher[0], receiverPrivateKeys[0][indice], receiverPublicKeys[0], G, GAff, fft, multi)

		// Statement computation, ie x = f(rd, receiverPrivateKeys[indice], polEnc)
		UA,VA,WA := zk.ComputeFunctionH(n ,receiverPublicKeys[0], Evala[i], rda[i], receiverPrivateKeysindice[0], cipherindice1[0],  cipherindice2[0], G)
		X1a[i]=curve.BatchJacobianToAffineG1(UA)
		X2a[i]=curve.BatchJacobianToAffineG1(VA)
		(&rdPKEnca).FromJacobian(&WA)
		}
		

	
	for a:=0; a<B; a++{	
		resharedCipher[a]=resharedCiphera
		rd[a]=rda
		polEnc[a]=polEnca
		Eval[a]=Evala
		X1[a]=X1a
		X2[a]=X2a
		rdPKEnc[a]=rdPKEnca
	}
	c, z1, z2, X1Star, X2Star, rdPKStar:= zk.ComputeProofH(dataProof, t, n, b, mm, cipherindice, receiverPublicKeys, rd, polEnc, resharedCipher, receiverPrivateKeysindice,	X1, X2, rdPKEnc, G, GAff, fft, RandomChallenge)

	LP:= zk.ComputePointsVec(n,t,mm,X1,X2,rdPKEnc,X1Star,X2Star,rdPKStar)
	start:=time.Now()
	r, vecr, vecrj, sumj := zk.PrecomputeScalarsH(n,t,b)

	start = time.Now()
	zk.VerifyProofH(dataProof, n, t, b, mm,  c , z1 , z2,  X1, X2, rdPKEnc, X1Star, X2Star, rdPKStar, LP, receiverPublicKeys, cipherindice1, cipherindice2, G, r, vecr, vecrj, sumj, RandomChallenge)
	return time.Since(start)
}


func test_NIZKVerifyfinal(n *big.Int, t *big.Int, b *big.Int, W *big.Int, indice int, fft bool, multi bool) (bool, bool) {
	RandomChallenge := true
	B:=int(b.Int64())
	index := make([]int,  int(t.Int64()) +1)
	mm:=make([]*big.Int,B)
	for i:=range(mm){
		mm[i]=W
	}
	for j := 0; j < int(t.Int64()) +1; j++ {
		index[j] = j+1
	}
	
	var instance []*cryptofr.CommitteeSharingInstance
	var receiverPublicKeys [][]curve.G1Jac
	var receiverPrivateKeys [][]*fr.Element
	var G curve.G1Jac
	var GAff curve.G1Affine
	
	for a:=0; a<B; a++{
	instance = append(instance,cryptofr.NewCommitteeSharingInstance(t, n ,index, fft))
	receiverPublicKeys = append(receiverPublicKeys, cryptofr.GetPublicKeys(instance[a].ReceiverKeys))
	receiverPrivateKeys = append(receiverPrivateKeys, cryptofr.GetPrivateKeys(instance[a].ReceiverKeys))
	G = instance[a].G
	GAff = instance[a].GAff
	}

	var M fr.Element
	var S curve.G1Jac
	var MInt big.Int
	var msg int64
	var dataProof zk.ProofData
	dataProof.H = sha256.New()
		
	msg=int64(7)
	M.SetInt64(msg) //.FromMont()
	M.BigInt(&MInt)
	cipher:=make([][]cryptofr.CipherMR,B)
	cipherindice:=make([]cryptofr.CipherMR,B)
	cipherindice1:=make([]curve.G1Jac, B)
	cipherindice2:=make([]curve.G1Jac, B)
	receiverPrivateKeysindice:=make([]*fr.Element, B)
	for a:=0; a<B; a++{
	S.ScalarMultiplication(&G, &MInt)
	cipher[a], _, _, _ = cryptofr.EncryptMR(dataProof.H, t , n,  receiverPublicKeys[a],  S, G, GAff, fft, multi)
	cipherindice[a]=cipher[a][indice]
	cipherindice1[a]=cipherindice[a].C1
	cipherindice2[a]=cipherindice[a].C2
	receiverPrivateKeysindice[a]=receiverPrivateKeys[a][indice]
	}


	resharedCipher:=make([][][]cryptofr.CipherMR,B)
	rd:=make([][][]*fr.Element,B)
	polEnc:=make([][][]fr.Element,B)
	Eval:=make([][][]curve.G1Jac,B)
	X1:=make([][][]curve.G1Affine,B)
	X2:=make([][][]curve.G1Affine,B)
	rdPKEnc := make([]curve.G1Affine,B)
	
	for a:=0; a<B; a++{
		Wint:=int(W.Int64())
		resharedCiphera:=make([][]cryptofr.CipherMR,Wint)
		rda:=make([][]*fr.Element,Wint)
		polEnca:=make([][]fr.Element,Wint)
		Evala:=make([][]curve.G1Jac,Wint)
		X1a:=make([][]curve.G1Affine,Wint)
		X2a:=make([][]curve.G1Affine,Wint)
		var rdPKEnca curve.G1Affine
	
		
		for i:=0; i<Wint; i++{
		resharedCiphera[i], rda[i], polEnca[i], Evala[i] = cryptofr.ReshareMR(dataProof.H, t , n, indice, cipher[a], receiverPrivateKeys[a][indice], receiverPublicKeys[a], G, GAff, fft, multi)

		// Statement computation, ie x = f(rd, receiverPrivateKeys[indice], polEnc)
		UA, VA, WA:= zk.ComputeFunctionH(n ,receiverPublicKeys[a], Evala[i], rda[i], receiverPrivateKeysindice[a], cipherindice1[a],  cipherindice2[a], G)
		X1a[i]=curve.BatchJacobianToAffineG1(UA)
		X2a[i]=curve.BatchJacobianToAffineG1(VA)
		(&rdPKEnca).FromJacobian(&WA)
		}
		
		cipherindice1[a]=cipher[a][indice].C1
		cipherindice2[a]=cipher[a][indice].C2
		resharedCipher[a]=resharedCiphera
		rd[a]=rda
		polEnc[a]=polEnca
		Eval[a]=Evala
		X1[a]=X1a
		X2[a]=X2a
		rdPKEnc[a]=rdPKEnca
	}
	c, z1, z2,X1Star, X2Star, rdPKStar:= zk.ComputeProofH(dataProof, t, n, b, mm, cipherindice, receiverPublicKeys, rd, polEnc, resharedCipher, receiverPrivateKeysindice,	X1, X2, rdPKEnc, G, GAff, fft, RandomChallenge)
	LP:= zk.ComputePointsVec(n,t,mm,X1,X2,rdPKEnc,X1Star,X2Star,rdPKStar)



	r, vecr, vecrj, sumj := zk.PrecomputeScalarsH(n,t,b)
	start:=time.Now()
	res1,res2:= zk.VerifyProofH(dataProof, n, t, b, mm,  c , z1 , z2,  X1 , X2 , rdPKEnc, X1Star, X2Star, rdPKStar, LP, receiverPublicKeys, cipherindice1, cipherindice2, G, r, vecr, vecrj, sumj, RandomChallenge)
	fmt.Println("Verification time:", time.Since(start))
	
	return res1,res2
}
